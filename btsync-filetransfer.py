#!/usr/bin/python

import subprocess, time

hash = "70a4b9f4707d258f559f91615297a3ec"
command = "md5sum sync/big.txt"

while (1==1):
	process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
	output = process.communicate()[0].split(" ")[0]
	if (output == hash):
		print time.ctime()
		file = open("time.txt", "w")
		file.write(time.ctime())
		file.close()
		break
	time.sleep(1)
